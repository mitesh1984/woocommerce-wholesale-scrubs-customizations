/*
 * JavaScript for Wholesale Scrub Sets Admin
 * Author: Luminary WS
 * Version: 0.0.1
*/

(function( $ ) {
  'use strict';

  $(document).ready(function(){

    // Email a packing slip to the supplier
    $(document).on('click','.scrubs_packing_slip_button',function(e){
      e.preventDefault();
      var order_id =    $('#post_ID').val(),
          task =        $(this).data('task');

      //console.log(task);
      //console.log(order_id);

      var data = {
        action     :    'scrubs_create_packing_slip',
        order_id   :    order_id,
        task       :    task
      };

      $.post( '/wp-admin/admin-ajax.php', data, function( response ) {
        if (response.success) {
          console.log(response);
          $('.packing-slip-response').html('Email sent to you.');
        } else {
          $('.packing-slip-response').html('There was an issue emailing the supplier. Please contact support.');
          console.log('error');
          console.log(response);
        }
      });

    });

  });

})( jQuery );
