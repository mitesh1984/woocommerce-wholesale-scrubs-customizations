<?php
/*
 * Plugin Name:       WooCommerce - Wholesale Scrubs Customizations
 * Plugin URI:        http://wholesalescrubsets.com
 * Description:       Customizations to WooCommerce for the Wholesale Scrub Sets website.
 * Version:           0.0.1
 * Author:            Luminary Web Strategies
 * Author URI:        http://luminary.ws
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
*/

/**
 * Init
 *
 * @since    0.0.1
 */
function woocommerce_scrubs_init() {
  require_once plugin_dir_path( __FILE__ ) . 'woocommerce-scrubs-class.php';
  require_once plugin_dir_path( __FILE__ ) . 'includes/PHPExcel.php';

	return Woocommerce_Scrubs::get_instance();

}
add_action( 'plugins_loaded', 'woocommerce_scrubs_init' );

 ?>
