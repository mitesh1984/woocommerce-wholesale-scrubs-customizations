<?php
/**
 * The file that defines the core plugin class
*/


class Woocommerce_Scrubs {

  /**
   * Single instance of plugin
   *
   * @var
   * @since  0.0.1
   */
  protected static $single_instance = null;

  public function __construct() {
    add_action( 'wp_ajax_scrubs_add_to_cart', array( $this, 'scrubs_add_to_cart_callback') );
    add_action( 'wp_ajax_nopriv_scrubs_add_to_cart', array( $this, 'scrubs_add_to_cart_callback') );
    add_action( 'woocommerce_checkout_process', array( $this, 'wc_minimum_order_amount') );
    add_action( 'woocommerce_before_cart' , array( $this, 'wc_minimum_order_amount') );

    add_action( 'wp_ajax_scrubs_create_packing_slip', array( $this, 'scrubs_create_packing_slip_callback') );
    add_action( 'init', array( $this, 'scrubs_create_packing_slip_callback') );

    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {

        // Queue JS for Admin
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

        // adds the supplier filtering dropdown to the orders view
        add_action( 'restrict_manage_posts', array( $this, 'add_supplier_dropdown' ) );
        add_filter( 'parse_query', array( $this, 'add_supplier_taxonomy' ) );

        // Add Packing Slip Meta Box on Orders
        add_action ('add_meta_boxes', array( $this, 'orders_add_packing_slip_box' ) );


        //add_filter( 'posts_join',  array( $this, 'add_order_items_join' ) );
        //add_filter( 'posts_where', array( $this, 'add_order_filterable_where' ) );
    }

  }

  /**
   * Creates or returns an instance of this class.
   *
   * @since  0.0.1
   * @return A single instance of this class.
   */
  public static function get_instance() {
    if ( null === self::$single_instance ) {
      self::$single_instance = new self();
    }

    return self::$single_instance;
  }

  /**
   * Adds items and variations to the cart
   *
   * @since  0.0.1
   * @return A single instance of this class.
  */
  public function scrubs_add_to_cart_callback() {

    check_ajax_referer( 'scrubs_cart', 'secure' );

    global $woocommerce;

    $cart_item_keys =         array();

    $productID =              $_POST['productID'];

    // Multiple values expected
    $variation_id =           explode(',',substr($_POST['variation_id'], 1));
    $quantity =               explode(',',substr($_POST['quantity'], 1));
    $attr_color =             explode(',',substr($_POST['attr_color'], 1));
    $attr_size =              explode(',',substr($_POST['attr_size'], 1));

    for($i=0;$i<count($variation_id);$i++) {
      if ( !empty($variation_id) ) {
        $variation_attributes =   array( 'attribute_pa_color' => $attr_color[$i], 'attribute_pa_size' => strtoupper($attr_size[$i]) );
        $cart_item_keys[] =       $woocommerce->cart->add_to_cart( absint( $productID ), $quantity[$i], $variation_id[$i], $variation_attributes, array() );
      }
    }

    wp_send_json_success( $cart_item_keys );

  }

  /**
   * Creates Packing Slip
   *
   * @since  0.0.1
   * @return A single instance of this class.
  */
  public function scrubs_create_packing_slip_callback() {

    global $woocommerce;

    /*$task =         (isset($_POST['task'])) ? $_POST['task'] : 'email';
    $order_id =     (isset($_POST['order_id'])) ? $_POST['order_id'] : false;*/
    if ( !isset($_GET['task']) && !isset($_POST['task']) ) {
      return;
    }
    $task =         (isset($_GET['task'])) ? $_GET['task'] : $_POST['task'];
    $order_id =     (isset($_GET['post'])) ? $_GET['post'] : $_POST['order_id'];

    if ( !$task || $task == '' ) {
      $task = 'download';
    }

    if ( !$order_id ) {
      wp_die('error');
      wp_send_json_error( array( 'message' => 'No Order ID provided.' ) );
    }

    $order =                            new WC_Order($order_id);
    $orderitems =                       $order->get_items();
    $order_information =                array();
    $order_information['order_id'] =    $order_id;
    $order_information['order_date'] =  explode(' ',$order->order_date);
    $order_information['order_date'] =  $order_information['order_date'][0];
    $order_information['customer'] =    explode("\n", str_replace( '<br/>', "\n", $order->get_formatted_shipping_address() . "\n" . $order->billing_phone));
    $order_information['items'] =       array();

  // grabbing customer information via post_meta

  $order_meta = get_post_meta($order_id);

  // format the phone number

  if (strlen($order_meta['_billing_phone'][0]) == 10) {
    $phone_number_formatted = substr($order_meta['_billing_phone'][0], 0, 3)."-".substr($order_meta['_billing_phone'][0], 3, 3)."-".substr($order_meta['_billing_phone'][0],6);
  } else {
    $phone_number_formatted = $order_meta['_billing_phone'][0];
}

    $objPHPExcel =                      new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("wholesalescrubsets")
                   ->setLastModifiedBy("WooCommerce")
                   ->setTitle("PHPExcel Test Document")
                   ->setSubject("PHPExcel Test Document")
                   ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                   ->setKeywords("office PHPExcel php")
                   ->setCategory("Test result file");

    $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri')
                  ->setSize(10);

    $objPHPExcel->getActiveSheet()->mergeCells('A1:P1');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Packing Slip');
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'THANK YOU FOR ORDERING FROM WHOLESALESCRUBSETS.COM');
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A3', 'IF YOU HAVE ANY QUESTIONS PLEASE CALL US 1-888-501-7319');
    $objPHPExcel->setActiveSheetIndex(0)
                ->getStyle('A1')->getFont()->setSize(20);

    $objPHPExcel->getActiveSheet()->getStyle('A1')
                                  ->applyFromArray(array('alignment'=>array('horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('N2', 'Date Added:')
                ->setCellValue('O2', $order_information['order_date'])
                ->setCellValue('N3', 'Order ID:')
                ->setCellValue('O3', $order_information['order_id']);

    $objPHPExcel->getActiveSheet()->getStyle('N2')
                                  ->applyFromArray(array('font'=>array('bold'=>true)));

    $objPHPExcel->getActiveSheet()->getStyle('N3')
                                  ->applyFromArray(array('font'=>array('bold'=>true)));

    $objPHPExcel->getActiveSheet()->getStyle('O2')
                                  ->applyFromArray(array('alignment'=>array('horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)));

    $objPHPExcel->getActiveSheet()->getStyle('O3')
                                  ->applyFromArray(array('alignment'=>array('horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)));
    $address_row = 6;

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5', 'Ship to');
    if ($order_meta['_shipping_company'][0] !== '') {
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $address_row, htmlspecialchars_decode($order_meta['_shipping_company'][0]));
      $address_row++;
    }

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'. $address_row, htmlspecialchars_decode($order_meta['_shipping_first_name'][0]).' '.htmlspecialchars_decode($order_meta['_shipping_last_name'][0]));
    $address_row++;

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $address_row, htmlspecialchars_decode($order_meta['_shipping_address_1'][0]));
    $address_row++;

    if ($order_meta['_shipping_address_2'][0] !== '') {
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $address_row, htmlspecialchars_decode($order_meta['_shipping_address_2'][0]));
    $address_row++;
    }

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'  . $address_row, htmlspecialchars_decode($order_meta['_shipping_city'][0]).' '.htmlspecialchars_decode($order_meta['_shipping_state'][0]).' '.htmlspecialchars_decode($order_meta['_shipping_postcode'][0]).' '.htmlspecialchars_decode($order_meta['_shipping_country'][0]));
    $address_row++;

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'  . $address_row, htmlspecialchars_decode($phone_number_formatted));
    $objPHPExcel->getActiveSheet()->getStyle('A' . $address_row)
                                  ->applyFromArray(array('alignment'=>array('horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));


    $objPHPExcel->getActiveSheet()->mergeCells('A6:P6');
    $objPHPExcel->getActiveSheet()->mergeCells('A2:L2');
    $objPHPExcel->getActiveSheet()->mergeCells('A3:L3');
    $objPHPExcel->getActiveSheet()->mergeCells('A7:P7');
    $objPHPExcel->getActiveSheet()->mergeCells('A8:P8');
    $objPHPExcel->getActiveSheet()->mergeCells('A9:P9');
    $objPHPExcel->getActiveSheet()->mergeCells('A10:P10');
    $objPHPExcel->getActiveSheet()->mergeCells('A11:P11');
    $objPHPExcel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $objPHPExcel->getActiveSheet()->getStyle('A5')
                                  ->applyFromArray(array('font'=>array('bold'=>true)));

    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFE7EFEF');
    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	
	$colors = $excel_items = array();
	$max_size_attr = 0;
    foreach( $orderitems as $item ) {
		$current_color 	= $variation_sku	= '';
		$product_id 	= $item['product_id'];
		$product_name 	= $item['name'];
		$qty			= $item['qty'];
		$color			= isset( $item['pa_color'] ) && !empty($item['pa_color']) ? $item['pa_color'] : '';	
		$size			= isset( $item['pa_size'] ) && !empty($item['pa_size']) ? $item['pa_size'] : 0;
		$backorder		= isset( $item['Backordered'] ) ? $item['Backordered'] : 0;
		$variation_id 	= isset( $item['variation_id'] ) && !empty( $item['variation_id'] ) ? $item['variation_id'] : 0;
		$availabel_qty  = $availabel_size = $actual_size_name = array();
		
		if( !isset( $excel_items[ $product_id ]['availabel_size'] ) ){
			$pa_size_list = get_the_terms( $product_id, 'pa_size');
			if( is_array( $pa_size_list ) && count( $pa_size_list ) ){
				foreach( $pa_size_list as $pa_size ){
					$size_slug = strtoupper($pa_size->slug);
					$availabel_size[]	=  	$size_slug;
					$actual_size_name[ $size_slug ]	= strtoupper($pa_size->name);
				}
			}
			if( $max_size_attr <= count( $availabel_size ) ){
				$max_size_attr = count( $availabel_size ) + 1;
			}
		}
		
		if(isset($variation_id) && !empty($variation_id)){
			$variation =            new WC_Product_Variation($variation_id);
			$variation_metadata =   $variation->get_variation_attributes();
		  	$variation_sku =        $variation->get_sku();
		  	$current_color =        get_term_by( 'slug', $variation_metadata['attribute_pa_color'], 'pa_color' );
		  	$current_size =         get_term_by( 'slug', $variation_metadata['attribute_pa_size'], 'pa_size' );
			$colors[ $variation_metadata['attribute_pa_color'] ] = $current_color->name;
	  	}
		if( is_array( $availabel_size ) && count( $availabel_size ) ){
			$excel_items[ $product_id ]['availabel_size'] = $availabel_size; 	
			$excel_items[ $product_id ]['actual_size_name'] = $actual_size_name; 	
		}
		$excel_items[ $product_id ]['name']	= get_the_title( $product_id );
		$excel_items[ $product_id ]['sku']	= $variation_sku;	
		if( $color ){
			$excel_items[ $product_id ]['color'][ $color ][ $size ] = $qty - $backorder;
		}else{
			$excel_items[ $product_id ]['qty']	= $qty - $backorder;
		}
	 	 
    }
	//die();
    $row=13; // The row for excel file.
    foreach( $excel_items as $item ) {
      $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A' . $row, $item['name']);

      $objPHPExcel->getActiveSheet()->getStyle('A'.$row)
                                    ->applyFromArray(array('font'=>array('bold'=>true)))
									->getFont()->setSize(14);
	  
	  
	  $alphas = range('A', 'Z');
	  $count = 0;
	  if( isset( $item['availabel_size'] ) && is_array( $item['availabel_size'] ) ){
		  $row++;
		  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, 'STYLE-COLOR' );
	  	  foreach( $item['availabel_size'] as $key => $size ){
	  	  	  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, $item[ 'actual_size_name' ][ $size ] );
	  	  }
		  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$max_size_attr] . $row, 'TOTALS' );
		  $objPHPExcel->getActiveSheet()
  						->getStyle('A'.$row.':'.$alphas[$max_size_attr].$row)
  						->getBorders()
  						->getAllBorders()
  						->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		  $row++;
	  }else if( isset( $item['color'] ) && is_array( $item['color'] ) ){
		  $row++;
		  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, 'STYLE-COLOR' );
	  	  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$max_size_attr] . $row, 'TOTALS' );	
		  $objPHPExcel->getActiveSheet()
  						->getStyle('A'.$row.':'.$alphas[$max_size_attr].$row)
  						->getBorders()
  						->getAllBorders()
  						->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		  $row++;
	  }
      // Create row of TOTALS
	  $sizewise_total = array();
	  
	  if( isset( $item['color'] ) && is_array( $item['color'] ) ){
	  	  foreach( $item['color'] as $key => $color_array ){
			  $count = 0;
			  $sku_name = !empty( $item['sku'] ) ? $item['sku'] .' - ' : '';
			  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, $sku_name.$colors[ $key ] );
			  $total = 0;

			  if( isset( $item['availabel_size'] ) && is_array( $item['availabel_size'] ) ){
			  	  foreach( $item['availabel_size'] as $key => $size ){
					  $qty = isset( $color_array[ $size ] ) ? $color_array[ $size ] : 0;
			  	  	  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, $qty );
					  $total += $qty;
					  if( isset( $sizewise_total[ $key ] ) ){
						  $sizewise_total[ $key ] = $sizewise_total[ $key ] + $qty;
					  }else{
						  $sizewise_total[ $key ] = $qty;
					  }
			  	  }
			  }else{
				  $total = $qty = $color_array[0];
				  if( isset( $sizewise_total[ 0 ] ) ){
					  $sizewise_total[ 0 ] = $sizewise_total[ 0 ] + $qty;
				  }else{
					  $sizewise_total[ 0 ] = $qty;
				  }
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, $qty );
			  }
			  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$max_size_attr] . $row, $total );
			  $row++;
	  	  }
	  }
	  $count = 0;
	  if( is_array( $sizewise_total ) && count( $sizewise_total ) ){
		  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, 'TOTALS' );
		  $size_total = 0;
		  foreach( $sizewise_total as $total ){
		  		$objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$count++] . $row, $total );
				$size_total += $total;
		  }
		  $objPHPExcel->setActiveSheetIndex(0)->setCellValue( $alphas[$max_size_attr] . $row, $size_total );
	  }else{
	      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphas[$max_size_attr] . $row, $item['qty']);
	  }
		$objPHPExcel->getActiveSheet()
						->getStyle('A'.$row.':'.$alphas[$max_size_attr].$row)
						->getBorders()
						->getAllBorders()
						->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

      
      $row++;
      $row++;

    }
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $packing_slip_file =    str_replace('woocommerce-scrubs-class.php', 'slips/'.$order_id.'.xls', __FILE__);
    $objWriter =            PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

    if ( $task == 'download' ) {
      $objWriter->save($packing_slip_file);
      header("Location: " . site_url() . "/wp-content/plugins/woocommerce-scrubs/slips/" . $order_id . ".xls");
      exit;
    } else {
      $objWriter->save($packing_slip_file);
      wp_mail( 'triport@sbcglobal.net', 'New order from Wholesale Scrub Sets (order #'.$order_id.')', 'A new order has been placed at Wholesale Scrub Sets. Please contact us at sales@wholesalescrubsets.com if there are any issues or items out of stock.', array('From: Wholesale Scrub Sets <sales@wholesalescrubsets.com>'), $packing_slip_file );
    }

    $objPHPExcel->disconnectWorksheets();
    unset($objPHPExcel);

    wp_send_json_success( $order_information );

  }

  // Copied verbatim from: https://docs.woothemes.com/document/minimum-order-amount/
  function wc_minimum_order_amount() {
      // Set this variable to specify a minimum order value
      $minimum = 150;

      if ( count(WC()->cart->cart_contents) > 1 && WC()->cart->subtotal < $minimum ) {
          if( is_cart() ) {
              wc_print_notice(
                  sprintf( 'You must have an order with a minimum of %s to place your order, your current order total is %s.' ,
                      wc_price( $minimum ),
                      wc_price( WC()->cart->subtotal )
                  ), 'error'
              );
          } else {
              wc_add_notice(
                  sprintf( 'You must have an order with a minimum of %s to place your order, your current order total is %s.' ,
                      wc_price( $minimum ),
                      wc_price( WC()->cart->subtotal )
                  ), 'error'
              );
          }
      } elseif ( count(WC()->cart->cart_contents) == 1 && WC()->cart->subtotal > $minimum) {

      } elseif ( count(WC()->cart->cart_contents) == 1 ) {
        foreach ( WC()->cart->cart_contents as $product ) { // Loop through all cart contents
          $sample_product = get_post_meta( $product['product_id'], 'sample_product', true );
        }

        if ( $sample_product != 'yes' ) { // If sample product is found, show checkout button
          if( is_cart() ) {
              wc_print_notice(
                  sprintf( 'You must have an order with a minimum of %s to place your order, your current order total is %s.' ,
                      wc_price( $minimum ),
                      wc_price( WC()->cart->subtotal )
                  ), 'error'
              );
          } else {
              wc_add_notice(
                  sprintf( 'You must have an order with a minimum of %s to place your order, your current order total is %s.' ,
                      wc_price( $minimum ),
                      wc_price( WC()->cart->subtotal )
                  ), 'error'
              );
          }
        }
      }
  }

  public function enqueue_admin_scripts() {
    wp_enqueue_script( 'woocommerce-scrubs-admin', plugins_url() . '/woocommerce-scrubs/javascripts/admin.js', 'jQuery' );
    wp_localize_script( 'woocommerce-scrubs-admin-jsvars', 'scrubs_woocommerce_admin', array( 'site_url' => home_url(), 'ajax_url' => admin_url( 'admin-ajax.php' ), 'nonce' => wp_create_nonce( 'scrubs_admin' ) ) );
  }

  public function orders_add_packing_slip_box() {
    add_meta_box( 'scrubs-packing-slip', __( 'Packing Slip', 'textdomain' ), 'Woocommerce_Scrubs::output_packing_slip_box', 'shop_order', 'side', 'low' );
  }

  public static function output_packing_slip_box(){
    global $post;
    echo '<p>The packing slip is a generated Excel (.xls) document. Choose to email it to the supplier or download it locally.</p>';
    echo '<div class="packing-slip-response"></div>';
    //echo '<p><a href="#" data-task="email" class="scrubs_packing_slip_button button">Email to Supplier</a></p>';
   // echo '<p><a href="?post='.$post->ID.'&action=edit&task=email" data-task="email" class="scrubs_packing_slip_button button">Email to Supplier</a></p>';
    echo '<p><a href="?post='.$post->ID.'&action=edit&task=email" data-task="email" class="scrubs_packing_slip_button button">Email To Supplier</a></p>';

    //echo '<p><a href="#" data-task="download" class="scrubs_packing_slip_button button">Download</a></p>';
    echo '<p><a href="?post='.$post->ID.'&action=edit&task=download" data-task="download" class="button">Download</a></p>';
  }

  public function add_supplier_taxonomy($query) {
    global $pagenow, $typenow;
    $qv = &$query->query_vars;
    if ($pagenow=='edit.php' && $typenow=='product') {
      if ( !isset($qv['taxonomy']) ) { $qv['taxonomy'] = 'pa_supplier'; }
    }
  }

  public function add_supplier_dropdown() {

    global $typenow;
    if ( $typenow == 'product' ) {

      $suppliers = get_terms( array('taxonomy' => 'pa_supplier', 'hide_empty' => false ) );
      $taxonomy = get_taxonomy('pa_supplier');

      if ( !empty($suppliers) ) :

        if ( $typenow == 'product' ) : ?>
              <select name="<?php echo 'term'; ?>" id="dropdown_suppliers">
        <?php else : ?>
              <select name="_supplier" id="dropdown_suppliers">
        <?php endif; ?>
                <option value=""><?php _e( 'Filter by Supplier', 'wc-filter-orders' ); ?></option>

                <?php foreach ( $suppliers as $supplier ) :
                  if ( $_GET['term'] == $supplier->slug ) {
                    $selected = 'selected="selected"';
                  } else {
                    $selected = '';
                  }
                  ?>
                <option value="<?php echo $supplier->slug . '" ' . $selected; ?>>
                  <?php echo $supplier->name; ?>
                </option>
                <?php endforeach; ?>
              </select>
          <?php
        endif;
      } else {
        return;
      }
   }

   public function add_order_items_join( $join ) {
    global $typenow, $wpdb;
    if ( 'shop_order' != $typenow ) {
      return $join;
    }

    if ( ! empty( $_GET['_supplier'] ) ) {
      $join .=  "
        LEFT JOIN {$wpdb->prefix}woocommerce_order_items woi ON {$wpdb->posts}.ID = woi.order_id,
        LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta woim ON woim.meta_key = '_product_id',
        LEFT JOIN {$wpdb->prefix}term_relationships term_relationships ON term_relationship.object_id = woim.object_id AND term_relationship.term_taxonomy_id = terms.term_id";
      //$join .= "
        //LEFT JOIN {$wpdb->prefix}woocommerce_order_item_meta woim ON {$wpdb->posts}.ID = woim._product_id"
    }
    return $join;
  }

  public function add_order_filterable_where( $where ) {
    global $typenow, $wpdb, $wc_coupon_names;
    return $where;
    if ( 'shop_order' != $typenow ) {
      return $where;
    }

    if ( ! empty( $_GET['_supplier'] ) ) {

      // Main WHERE query part
      $where .= $wpdb->prepare( " AND wp_terms.term_id = ".$_GET['_supplier'] );
    }

    return $where;
  }


}


?>
